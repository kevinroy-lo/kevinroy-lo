---
title:  Thinking-Faith-The-Crown
date: 2019-12-17 21:29:45
tags: ["Movie","The Crown"]
categories:
 - 生活点滴
 - 成长感悟
copyright: true
reward: true
rating: false
related_posts: false
---
![season02-thumb.jpg](https://i.loli.net/2019/12/17/TqMcxaSYRE6sLgA.jpg "The Crown")

{% note info %}
Find yourself a <b>faith</b>.<br/>
It helps. No...<br/>
Not just helps.<br/>
It's everything.<br/>
{% endnote %}

# 序
很长时间都没更新博客了，其实域名都在，只是时间都不知道溜哪里去了，最大的原因，还是自己懒起来了吧。当然也没想到，让我重新提笔竟然是这部英剧(The Crown)。它讲述了伊丽莎白二世的故事，从一个小姑娘，到挑起大任，管理国家事务。这是历史使然，也是个人成长。当生活当头重棒给你一击，压着你不得不前进的时候，有惊慌失措，但更多的，是收获。我期待一场野蛮式的成长，但它还没出现的时候，用心去捕捉生活的每一个想法，感悟。尽管看似散漫，但却是内心的想法，而想法成就一个人。我相信随着时间积累，总会得到一些令人意想不到的收获。fighting~

<!-- more -->

# Faith
 这是The Crown第 3 季第 4 集中出现的一幕，`Philip(译:菲利普)`的母亲问他，你的信仰是什么，他回答`Dormant`(类似睡觉的意思)。可见当时的菲利普已经丢失了自己的信仰。而她母亲的点醒了他，让他去寻找自己的 `Faith`。后来，菲利普在看到英国阿波罗登月时的电视直播而泪流满面，其一他自己也是飞机员，他本也应该有机会去月球漫步，记录这历史的一刻，但他不能，他是亲王也是丈夫，得守护女王。他为这三个年经的宇航员冒着生命危险成功登月而高兴，并有机会去单独采访这三名宇航员，并想在他们的回答中发现是什么支撑着他们登月，信仰？当他问及跑上月球是什么样的感受，并得到的回答是，月球上什么都没有，只有灰烬的时候。菲利普知道他已经得不到令他满意的回答了，这三个宇航员只是把危险的登月计划看作是一项任务，突破极限和探险太空并不是他们的信仰。
 
电视到最后，我不知道后来菲利普有没有找到自己的信仰，但他和那个教会的朋友(起初被他认为是无所是事的人)成了一生的朋友。而探索`Faith`也成了他一生中最引以为荣的事情。

我也不知道为什么电视剧中的这一段，即便从铺垫到讲述再到结束这个话题，即使穿越了好几集，我都很对它记忆犹新，甚至很自然的看出菲利普的内心情感。最终替他找到共同探索的志友而感到释然和高兴。

大概，我也是那个失去了`Faith`，或者从来就没有过的人吧。
 
 {% cq %}
  **假若一个人没有信仰，即使他们登上了月球，达到了目标，他们看到的也只有虚无和灰烬。**
 {% endcq %}
 
 # 剧词
 {% note info %}
 My mother died recently.<br/>
 She... she saw that something was amiss.<br/>
 It's a good word, that.A-Amiss.<br/>
 She saw that something was missing in her youngest child.<br/>
 Her only son.Faith.<br/>
 "How's your faith?" she asked me.<br/>
 I'm here to admit to you that...I've lost it.<br/>
 And...without it, what is there?<br/>
 The loneliness and emptiness and anticlimax of going all that way to the moon to find nothing,but haunting desolation...ghostly silence...gloom.<br/>
 
 That is what faithlessness is.<br/>
 
 As opposed to finding...wonder, ecstasy, the miracle of...divine creation, God's design and purpose.<br/>
 What am I trying to say?<br/>
 
 I'm trying to say that the solution to our problems,
 I think, is not in the ingenuity of the rocket,
or the science or the technology or...even the bravery.
No, the answer is in here.Or here, or wherever it is that faith resides.
And so... Dean Woods...having ridiculed you for what you and these poor, blocked, lost souls... were trying to achieve here in St. George's House...

I now find myself full of respect and admiration...and not a small part of...desperation...as I come to say...<br/>
Help.<br/>
Help me.
  {%endnote  %}

 # 结
 The Crown的最后几集真的非常的精彩，`Faith`只是其中的一个话题。有时间再写写其他感悟。
 最后，也推荐你们去观看。
 
 <div class="reference-linking">链接</div>
 - [The Crown(王冠)](https://movie.douban.com/subject/26427152/)
 
 
 
 
 