# Introduction

## 注意由于使用了`grup`压缩，请使用如下命令部署网站。

``` Js
npx hexo clean
npx hexo g
npx hexo douban -bm   // 生成豆瓣内容
npx gulp

npx hexo deploy
```
